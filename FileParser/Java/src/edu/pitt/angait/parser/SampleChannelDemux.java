package edu.pitt.angait.parser;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.channels.ScatteringByteChannel;

/**
 * The SampleChannelDemux class parses <i>.agt</i> files.
 * 
 * @author iMED Lab
 *
 */
public class SampleChannelDemux {
	
	/* Attributes */
	IntBuffer[] sampleBuffers; /** Sequence of buffers holding integer sample values. */
	ByteBuffer[] demuxedBytes; /** Sequence of buffers holding raw sample values. */
	int hyperSampleSize;
	int[] cumulativeSampleSize;
	int sampleCapacity;        /** Number of samples to convert (bytesPerChannel.length * 8). */
	int[] bytesPerChannel;     /** Array indicating the number of bytes for each channel. */
	long offset;               /** Offset in the demuxBytes buffers. */
	
	/**
	 * Creates a parser.
	 * @param bytesPerChannel Array containing the number of bytes for each ADC channels.
	 * @param sampleCapacity Number of samples to convert.
	 */
	public SampleChannelDemux(int[] bytesPerChannel, int sampleCapacity) {
		
		this.sampleCapacity = sampleCapacity;
		this.bytesPerChannel = bytesPerChannel;
		
		demuxedBytes = new ByteBuffer[bytesPerChannel.length]; // Sequence of buffers handling bytes (raw values)
		sampleBuffers = new IntBuffer[bytesPerChannel.length]; // Sequence of buffers handling integers (converted values)
		hyperSampleSize = 0;
		cumulativeSampleSize = new int[bytesPerChannel.length];
		
		// For each ADC channel
		for (int i = 0; i < bytesPerChannel.length; ++i)
		{
			// Initialize buffers from arrays
			demuxedBytes[i] = ByteBuffer.wrap(new byte[bytesPerChannel[i]]);
			sampleBuffers[i] = IntBuffer.wrap(new int[sampleCapacity]);
			hyperSampleSize += bytesPerChannel[i];
			cumulativeSampleSize[i] = hyperSampleSize;
		}
		
	}
	
	/**
	 * Parses a .agt file. 
	 * @param channel Java channel used to read an .agt file. 
	 * @return 2D array containing converted values. The first index is the ADC channel index; the second index is the sample in the channel.
	 * @throws IOException
	 */
	public int[][] parseInputStream(ScatteringByteChannel channel) throws IOException {
		
		int currentValue; // Temporary variable containing the converted value
		long lastRead = 0; // Number of bytes last read from the channel
		int relativeSampleCapacity = sampleCapacity;
		if(offset > 0)
			--relativeSampleCapacity;
		
		// Note this can block
		
		for (int i = 0;
			i < relativeSampleCapacity 
			&& (lastRead = channel.read(demuxedBytes,(int)offset,demuxedBytes.length-(int)offset)) > 0  // If the number of bytes read is greater than 0 (after a channel is read and content is stored in a sequence of buffers) 
			&& demuxedBytes[demuxedBytes.length-1].position() == demuxedBytes[demuxedBytes.length-1].limit(); // If last buffer is full
			++i)
		{
			
			// For each buffer
			for (int j = 0; j < demuxedBytes.length; ++j)
			{
				currentValue = 0;
				demuxedBytes[j].rewind(); // Go back to the beginning of the buffer in order to read its content
				// For each byte in the buffer
				for (int k = bytesPerChannel[j]-1; k>=0; --k) // Read buffer from left to right i.e. from MSB to LSB
				{
					currentValue += ((long)(demuxedBytes[j].get()<<(8*k)))&(0xFF<<(8*k)); // Retrieve byte and convert it to a long
																						   // The byte is shifted if it contains the MSB
				}
				demuxedBytes[j].clear(); // Clear the buffer
				sampleBuffers[j].put(currentValue); // Update the corresponding integer buffer
			}
			offset = 0;
		}
		
		int samplesTaken = sampleBuffers[sampleBuffers.length-1].position(); // Retrieve the position of the last integer buffer
		                                                                     // so we know how many samples were converted
		
		// For each integer buffer
		for (int i = 0; i < sampleBuffers.length; ++i) 
		{
			if (lastRead >= cumulativeSampleSize[i] && lastRead != cumulativeSampleSize[cumulativeSampleSize.length-1]) {
				++offset;
			}
			sampleBuffers[i].rewind();
		}
		// Return samples into nice integer array for processing.
		// NB This is highly inefficient for RT processing and should be changed to run over the IntBuffers
		
	    // Create integer array to be returned
		int[][] data = new int[sampleBuffers.length][samplesTaken];

		// Retrieve samples in buffers and put them in an array
		for (int i = 0; i < samplesTaken*sampleBuffers.length; i++) 
		{
			data[i%sampleBuffers.length][i/sampleBuffers.length] = sampleBuffers[i%sampleBuffers.length].get();
		}
		
		// Clear integer buffers
		for (int i = 0; i < sampleBuffers.length; ++i) 
		{
			sampleBuffers[i].clear();
		}
		
		// Return the table if samples were converted. Return null otherwise.
		if(samplesTaken > 0) {
			return data;
		} else {
			return null;
		}
	}
}
