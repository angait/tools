package edu.pitt.angait.parser.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Pipe;

//import org.junit.After;
//import org.junit.Before;
import org.junit.Test;
import edu.pitt.angait.parser.SampleChannelDemux;


public class SampleChannelDemuxTest {
	@Test
	public void basicInputOutputTestCase() {
		int[] bytesPerChannel = {2,2,1,1};
		SampleChannelDemux testDemux = new SampleChannelDemux(bytesPerChannel, 2);
		try {
			Pipe p = Pipe.open();
			byte[] testData = {0,0,0,1,2,3,0,4,0,5,6,7};
			ByteBuffer testBuffer = ByteBuffer.wrap(testData);
			Pipe.SinkChannel sink = p.sink();
			sink.write(testBuffer);
			Pipe.SourceChannel srcChan = p.source();
			
			int[][] data = testDemux.parseInputStream(srcChan);
			
			for(int i = 0; i < 8; ++i) {
				assertEquals(i,data[i%4][i/4]);
			}
		} catch (IOException ex) {
			fail(ex.getMessage());
		}
	}
	@Test
	public void repeatedInputOutputTestCase() {
		int[] bytesPerChannel = {2,2,1,1};
		SampleChannelDemux testDemux = new SampleChannelDemux(bytesPerChannel, 2);
		try {
			Pipe p = Pipe.open();
			byte[] testData = {0,0,0,1,2,3,0,4};
			byte[] testData2 = {0,5,6,7};
			ByteBuffer testBuffer = ByteBuffer.wrap(testData);
			ByteBuffer testBuffer2 = ByteBuffer.wrap(testData2);
			Pipe.SinkChannel sink = p.sink();
			sink.write(testBuffer);
			Pipe.SourceChannel srcChan = p.source();
			
			int[][] data = testDemux.parseInputStream(srcChan);
			
			for(int i = 0; i < 4; ++i) {
				assertEquals(i,data[i%4][i/4]);
			}
			
			sink.write(testBuffer2);
			data = testDemux.parseInputStream(srcChan);
			for(int i = 4; i < 8; ++i) {
				assertEquals(i,data[(i-4)%4][(i-4)/4]);
			}
			
		} catch (IOException ex) {
			fail(ex.getMessage());
		}
	}
}