
set FILE_PARSER_PATH=C:\Users\Joey\Dropbox\Research\angait\android\FileParser
set FILE=%~f1
set CONVERTED=%FILE%.csv
java -cp "%FILE_PARSER_PATH%/bin" edu.pitt.angait.parser.FileParser 2 8 8 %FILE% "%CONVERTED%"

:set SCRIPT_DIR=%~d0%~p0
:CMD /C "CD "%SCRIPT_DIR%" & matlab -nodesktop -nosplash -r  display_graphs('%CONVERTED%')"