#!/usr/bin/env python

import sys
from PyFileParser import parseBinFile, generateCSVFile

def usage():
    print("Usage: " + sys.argv[0] + " filename\n")

if __name__ == "__main__":
    if len(sys.argv) < 2 or len(sys.argv) > 2:
        usage()
    else:
        filename = sys.argv[1]
        res = parseBinFile(filename)
        #generateCSVFile(res, filename + ".adc")
        
        # Quick hack to convert ADC values
        # First retrieve reference voltage for acc
        accVref = 0
        for val in res[10]:
            accVref += val
        
        accVref /= float( len(res[10]) )
        accVref = round(accVref)
        del res[10] # Delete last column
        
        # Convert acceleration values
        tmp = []
        for i in range(7,10):
            for val in res[i]:
                tmp.append( (val - accVref) * 0.0022 )
            res[i] = tmp
            tmp = []
            
        # Convert values from ADC in uC
        tmp = []
        for i in range(7):
            for val in res[i]:
                tmp.append( val * (3.3/1023) )
            res[i] = tmp
            tmp = []
        
        # Write CSV file
        generateCSVFile(res, filename + ".csv")

