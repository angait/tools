#!/usr/bin/env python

import struct      # To convert bytes to integers

def parseBinFile(filename, nbChannel=11):
    """
    Parses a .agt binary file.
    
    Returns a dictionary containing the result of the parsing.
    """
     
    stop = False     # Controls parsing loop
    
    # Initialize the dictionary where the results will be stored
    res = {}
    for i in range(nbChannel):
         res[i] = [] # Empty list
    
    # Open file in binary mode
    with open(filename, "rb") as fileToConvert:
        while not stop:
            # Read two bytes
            data = fileToConvert.read(2)
            
            # If we reached the end of the file
            if data == '':
                stop = True
                continue
            
            # Convert bytes as unsigned short variables
            convData = struct.unpack(">H", data)
            convData = convData[0] # Extract single variable from tuple
            
            # Extract channel id from value
            channelID = (convData & 0xF000) >> 12
            
            # Extract measurement
            value = convData & 0x0FFF
            
            # Store measurement in an array
            res[channelID].append(value)
    
    # Return the results
    return res


def generateCSVFile(res, filename):
    """
    Generates a CSV file from the result of the parsing
    of a .agt binary file.
    """
    
    # Determine the longest list in the results
    maxLength = 0
    for k, val in res.items():
        if len(val) > maxLength:
            channel, maxLength = k, len(val)
    
    # Loop to add 'NaN' values if the list is too short
    for val in res.values():
        while len(val) < maxLength:
            val.append('NaN')
    
    
    # Get the length of a list of values for a channel
    N = len(res[0])
    
    with open(filename, 'w') as csvFile:
        for i in range(N):       # For each value
            line = ""
            for k in res.keys(): # For each channel
                line += str(res[k][i])
                
                if k != len(res.keys()) - 1: # If end of line is not reached
                    line += ', ' 
                else: # If end of line
                    line += '\n'
            
            # Write a line to the CSV file
            csvFile.write(line)
            

def generateBinFile(filename):
    """
    Generates a dummy .agt file.
    """
    
    # Generate simple values
    values = []
    for i in range(11):
        tmp = i | (i << 12)
        values.append(tmp)
    
    # Generate binary file
    with open(filename, "wb") as f:
        for i in range(64):
            for val in values:
                f.write( struct.pack(">H", val) )
    
if __name__ == "__main__":
    generateBinFile("dummy.agt")
    res = parseBinFile("dummy.agt")
    generateCSVFile(res, "dummy.csv")

            
