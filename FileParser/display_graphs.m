function display_graphs(filename)
% Function that imports data from a csv file and plot it.
% csv files are generated from agt files.
% Author: iMEDLab

% Import data
% Each column corresponds to an ADC channel
rawData = load(filename);

% Convert ADC values to voltages
channels = rawData(:,1:7);

acc = rawData(:,8:end);

data = [channels, acc];

% Plot each column
subplot(5, 2, 1); 
plot(data(:,1));
title('Respiratory belt');
 
subplot(5, 2, 2); 
plot(data(:,2));
title('ECG');

subplot(5, 2, 3); 
plot(data(:,3));
title('Electrodermal response');

subplot(5, 2, 4); 
plot(data(:,4));
title('Foot switch Right Back');

subplot(5, 2, 5); 
plot(data(:,5));
title('Foot switch Left Back');

subplot(5, 2, 6); 
plot(data(:,6));
title('Foot switch Right Front');

subplot(5, 2, 7); 
plot(data(:,7));
title('Foot switch Left Front');

subplot(5, 2, 8); 
plot(data(:,8));
title('X-Axis');

subplot(5, 2, 9); 
plot(data(:,9));
title('Y-Axis');

subplot(5, 2, 10); 
plot(data(:,10));
title('Z-Axis');

end
